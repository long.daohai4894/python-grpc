import os

import grpc
from flask import Flask, render_template

from store_pb2 import StoreListRequest
from store_pb2_grpc import StoreStub

app = Flask(__name__)

products = [
    {
        'id': 1,
        'name': 'Thắt Lưng Nam Dây Da Phong Cách Hàn Quốc Khóa Tự Động',
        'brand': 'TOPEE'
    },
    {
        'id': 2,
        'name': 'Bóp Ví Da Nam Hàn Quốc HQ2',
        'brand': 'OEM'
    },
    {
        'id': 3,
        'name': 'Ví da cầm tay nam nữ cao cấp',
        'brand': 'OEM'
    },
]

store_host = os.getenv("STORE_HOST", "localhost")
store_channel = grpc.insecure_channel(
    f"{store_host}:50051"
)
store_client = StoreStub(store_channel)


@app.route("/<int:product_id>")
def homepage(product_id):
    product = products[product_id - 1]
    store_request = StoreListRequest(
        product_id=product_id
    )
    store_response = store_client.Store(
        store_request
    )
    return render_template(
        "homepage.html",
        product=product,
        store_list=store_response.stores
    )
