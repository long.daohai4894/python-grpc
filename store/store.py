from concurrent import futures

import grpc

from store_pb2 import (
    StoreData, StoreListResponse
)
import store_pb2_grpc

product_ids = {
    1: [
        StoreData(id=1, name="DIZIZID", rate=4.6, price=52000),
        StoreData(id=2, name="Thời trang TOPE", rate=4.4, price=139000),
    ],
    2: [
        StoreData(id=1, name="Bảo Bảo Mart", rate=4.6, price=49000),
        StoreData(id=2, name="Hugo", rate=4, price=48900),
        StoreData(id=3, name="shopthuanphong", rate=4.4, price=52700),
    ],
    3: [
        StoreData(id=1, name="Pretty Store", rate=4.5, price=46000),
        StoreData(id=2, name="Hoàng Hà 888", rate=4, price=45000),
    ]
}


class StoreService(
    store_pb2_grpc.StoreServicer
):
    def Store(self, request, context):
        if request.product_id not in product_ids:
            context.abort(grpc.StatusCode.NOT_FOUND, "Product not found")

        store_list = product_ids[request.product_id]

        return StoreListResponse(stores=store_list)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    store_pb2_grpc.add_StoreServicer_to_server(
        StoreService(), server
    )
    server.add_insecure_port("[::]:50051")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    print('Start a server...')
    serve()