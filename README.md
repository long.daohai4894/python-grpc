# GRPC with Python
Example project for gRPC with Python
## Installation
```
$ virtualenv -p python3 .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```
### Generate file python_out and grpc_python_out
```
$ python -m grpc_tools.protoc -I ../proto --python_out=. --grpc_python_out=. ../proto/store.proto
```
### Run gRPC server
```
$ cd store
$ python store.py
```
### Run Flask
```
$ cd product
$ FLASK_APP=main flask run
```
